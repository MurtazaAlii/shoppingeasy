﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ShoppingEasy.Web.Startup))]
namespace ShoppingEasy.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
